# Rogue Quest
A text RPG game written in C++ using a terminal user interface (TUI).

## Quick start
Prerequisites: `Cmake`
### Unix
``` sh
# compiling options
./build.sh          # builds and runs the game
./build.sh --build  # only builds the executable
./build.sh --clean  # removes the build files

# running can be done in two ways:
./run
./build/roguequest
```

### Windows
Until further testing, WSL is the only option (i guess).
[WSL installation](https://docs.microsoft.com/en-us/windows/wsl/install)
