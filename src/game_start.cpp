#include "headers/game.hpp"

void Game::start() {
    using namespace ftxui;
    using namespace std::chrono_literals;

    int tab_index = 0;
    this->screen.Clear();

    /* ********************************
    ** PLAYER TAB
    *  ********************************/

    std::vector<std::string> weapons = this->player.getWeaponNames();
    Component weaponSelect = Radiobox(&weapons, &this->selectedWeapon);

    std::vector<std::string> armor = this->player.getArmorNames();
    Component armorSelect = Radiobox(&armor, &this->selectedArmor);

    auto Equipment = Container::Horizontal({weaponSelect, armorSelect});

    auto HP_stat = [&] {
        Elements line;
        line.emplace_back(text("  [ HP  ]  "  + std::to_string(this->player.getHP())                + "  "));
        return line;
    };
    auto ATK_stat = [&] {
        Elements line;
        line.emplace_back(text("  [ ATK ]  " + std::to_string(this->player.getATK(this->selectedWeapon)) + "  "));
        return line;
    };
    auto SPD_stat = [&] {
        Elements line;
        line.emplace_back(text("  [ SPD ]  " + std::to_string(this->player.getSPD(this->selectedWeapon)) + "  "));
        return line;
    };
    auto DEF_stat = [&] {
        Elements line;
        line.emplace_back(text("  [ DEF ]  " + std::to_string(this->player.getDEF(this->selectedArmor))  + "  "));
        return line;
    };
    auto EXP_stat = [&] {
        Elements line;
        line.emplace_back(text("  [ EXP ]  " + std::to_string(this->player.getEXP())                + "  "));
        return line;
    };
    auto GOLD = [&] {
        Elements line;
        line.emplace_back(text("  [ GLD ]  " + std::to_string(this->player.getGold())               + "  "));
        return line;
    };

    auto Player = Renderer(Equipment, [&] {
        auto weapon_win = window(text("Weapon") | bold,
                                 weaponSelect->Render() | hcenter | vscroll_indicator | frame);
        auto armor_win = window(text("Armor") | bold,
                                armorSelect->Render() | hcenter | vscroll_indicator | frame);
        return hbox({
            window(text("Stats") | bold, vbox({
                hflow(HP_stat()),
                hflow(ATK_stat()),
                hflow(SPD_stat()),
                hflow(DEF_stat()),
                hflow(EXP_stat()),
                hflow(GOLD()),
            }) | hcenter ),
            hbox({
                weapon_win | flex,
                armor_win | flex
            }) | flex,
        }) | flex_grow | border;
    });

    /* ********************************
    ** SHOP TAB
    *  ********************************/

    int s_weapon_selected = 0;
    s_weapons = this->shop.getWeaponNames();
    Component s_weaponSelect = Radiobox(&s_weapons, &s_weapon_selected) | size(WIDTH, EQUAL, 32);

    int s_armor_selected = 0;
    s_armor = this->shop.getArmorNames();
    Component s_armorSelect = Radiobox(&s_armor, &s_armor_selected) | size(WIDTH, EQUAL, 32);

    auto buy_wep_opt = ButtonOption();
    auto buy_arm_opt = ButtonOption();

    auto ShopItems = Container::Vertical({
        Container::Horizontal({
            s_weaponSelect,
            s_armorSelect,
        }) | hcenter,
        Container::Horizontal({
            Button(" [ BUY WEAPON ] ", [&] {
                if ( std::ranges::size(s_weapons) != 0 ) {
                    auto idx = s_weapon_selected;
                    auto cost = this->shop.getWepCost(idx);
                    if ( cost <= this->player.getGold() ) {
                        auto w = this->shop.buyWeapon(idx);
                        this->player.newWeapon(w);
                        s_weapons.erase(std::ranges::cbegin(s_weapons) + idx);
                        weapons = this->player.getWeaponNames();
                        s_weapons = this->shop.getWeaponNames();
                    }
                }
            }, &buy_wep_opt) | center,
            Button(" [ BUY ARMOR ] ",  [&] {
                if ( std::ranges::size(s_armor) != 0 ) {
                    auto idx = s_weapon_selected;
                    auto cost = this->shop.getArmCost(idx);
                    if ( cost <= this->player.getGold() ) {
                        auto a = this->shop.buyArmor(idx);
                        this->player.newArmor(a);
                        s_armor.erase(std::ranges::cbegin(s_armor) + idx);
                        armor = this->player.getArmorNames();
                        s_armor = this->shop.getArmorNames();
                    }
                }
            }, &buy_arm_opt) | center
        }) | flex | center
    });

    auto Shop = Renderer(ShopItems, [&] {
        auto s_win = window(text("Shop") | bold,
                            ShopItems->Render());

        return vbox({
            hbox({
                s_win | flex,
            }),
            window(text("Details") | bold, vbox({
                vbox({
                    hbox({
                        text("* " + this->shop.getWepInfo(s_weapon_selected)) | flex,
                        color(Color::Gold1,
                              text("  " + std::to_string(this->shop.getWepCost(s_weapon_selected)) + "g  ")) | align_right
                    }),
                    hbox({
                        text("* " + this->shop.getArmInfo(s_armor_selected)) | flex,
                        color(Color::Gold1,
                              text("  " + std::to_string(this->shop.getArmCost(s_armor_selected)) + "g  ")) | align_right
                    })
                })
            })),
        }) | hcenter | flex_grow | border;
    });

    /* ********************************
    ** WORLD TAB
    *  ********************************/

    int action_selected = 0;
    int location_selected = 0;
    this->eventMessage = this->welcome_msg;

    const std::vector<std::string> actions =
        { "Fight", "Fight the Boss", "Rest in the wild", "Rest in the Tavern" };
    Component actionSelect = Radiobox(&actions, &action_selected) | size(WIDTH, EQUAL, 30);

    const std::vector<std::string> locations = this->world.getLocationNames();
    Component locationSelect = Radiobox(&locations, &location_selected);

    auto action_button = ButtonOption();
    auto location_button = ButtonOption();

    auto WorldView = Container::Vertical({
        actionSelect,
        Button(" [ Perform Action ] ", [&] {
            if (this->in_fight) return;

            this->eventMessage = "Chosen action: " + actions[action_selected];

            if (action_selected == 0) {        // fight
                std::thread fight([&] {
                    this->fight(fightType::normal);
                });
                fight.detach();
            } else if (action_selected == 1) { // fight the boss
                std::thread fight([&] {
                    auto idx = this->world.getCurrLocationIndex();
                    if ( idx == 0 || (idx != 0 && this->world.defeatedBosses[idx-1]) ) {
                        if ( !this->world.defeatedBosses[this->world.getCurrLocationIndex()] ) {
                            this->fight(fightType::boss);
                        } else {
                            this->eventMessage = "You already have defeated this boss!";
                        }
                    } else {
                        this->eventMessage = "You have to defeat the previous boss first!";
                    }
                });
                fight.detach();
            } else if (action_selected == 2) { // rest in the wild
                this->rest();
            } else if (action_selected == 3) { // rest in the tavern
                this->restInTavern();
            } else if (action_selected == 4) { // ask for quests
                std::thread fight([&] {
                    return;
                    //this->fightOpponent(fightType::quest);
                });
                fight.detach();
            }
        }, &action_button) | hcenter,

        locationSelect,
            Button(" [ Travel ] ", [&] {
                 this->eventMessage = "Travelling to: " + locations[location_selected];
                 this->travel(location_selected);
            }, &location_button) | hcenter,
    });

    auto World = Renderer(WorldView, [&] {
        auto info = window(
            text("Info") | bold,
            hbox({
                text("Current Location: " +
                     this->world.getCurrLocationName() +
                     ", Boss: " +
                     ( this->world.defeatedBosses[this->world.getCurrLocationIndex()] ? "Defeated" : "Alive" )
                )
            }) | hcenter
        );

        auto action_win = window(text("Action") | bold,
                                 WorldView->Render() | size(WIDTH, EQUAL, 30) | hcenter | flex | vscroll_indicator | frame);

        auto mini_stats = hbox({
            color(Color::Green,  text(" [" + std::to_string(this->player.getHP())   + " / " + std::to_string(this->player.getmaxHP()) + " HP] ")  | bold),
            color(Color::Blue,   text(" [" + std::to_string(this->player.getEXP())                                                    + " EXP] ") | bold),
            color(Color::Yellow, text(" [" + std::to_string(this->player.getGold())                                                   + " G] ")   | bold),
        }) | hcenter;

        return vbox({
            info,
            hbox({
                action_win,
                window(text("Game Log") | bold, vbox({
                        hflow(paragraph(this->eventMessage)),
                    }) | size(WIDTH, EQUAL, 60) | yframe | flex) | flex,
            }) | flex_grow,
            window(text("Mini Stats") | bold, vbox({
                color(
                    Color::Red,
                    gauge(
                        float(this->player.getHP()) / float(this->player.getmaxHP())
                    ) | flex
                ),
                separator(),
                mini_stats,
            }) | flex)
        }) | border;
    });

    /* ********************************
    ** MENU TAB
    *  ********************************/

    auto new_game_button = ButtonOption();
    auto save_button =     ButtonOption();
    auto load_button =     ButtonOption();
    auto getSaves = []() {
        std::vector<std::string> out;
        for (auto const &save : fs::directory_iterator{fs::current_path() / "saves"}) {
            out.push_back(save.path().filename().string());
        }
        std::sort(std::ranges::begin(out), std::ranges::end(out));
        out.erase(std::ranges::begin(out));
        return out;
    };
    std::vector<std::string> saveNames = getSaves();
    int s_idx = 0;
    auto saveMenu = Menu(&saveNames, &s_idx);

    auto MenuView = Container::Vertical({
        Button(" [ NEW GAME ] ", [&] {
            this->load("*new*");
            tab_index = 0;
        }, &new_game_button) | hcenter,
        Button(" [ SAVE ] ", [&] {
            this->save(saveNames[s_idx]);
            tab_index = 0;
        }, &save_button) | hcenter,
        Button(" [ LOAD ] ", [&] {
            this->load(saveNames[s_idx]);
            tab_index = 0;
        }, &load_button) | hcenter,
        Button(" [ EXIT ] ", this->screen.ExitLoopClosure()) | bold | hcenter,
        saveMenu | hcenter
    });


    auto Menu = Renderer(MenuView, [&] {
        auto menu_win = window(text("Menu") | bold,
                               MenuView->Render() | hcenter | frame | flex);

        return vbox({
            menu_win | flex,
        }) | flex_grow | border;
    });

    /* ********************************
    ** FIGHT POP-UP
    *  ********************************/

    int enemy_selected = 0;
    std::vector<std::string> enemies_in_fight = this->getEnemies();
    Component enemySelect = Radiobox(&enemies_in_fight, &enemy_selected);

    auto attack_button = ButtonOption();
    auto done_button = ButtonOption();
    auto flee_button = ButtonOption();

    auto fightContainer = Container::Vertical({
        Container::Horizontal({
            enemySelect,
        }) | hcenter | flex_grow,
        Container::Horizontal({
            Button(" ATTACK ",
                   [&] { this->attackEnemy(enemy_selected); },
                   &attack_button),
            Button(" END TURN ",
                   [&] { this->attackPlayer(); },
                   &attack_button),
            Button(" FLEE ",
                   [&] { this->flee(); },
                   &attack_button),
        }) | hcenter,
    });

    auto FightPopUp = Renderer(fightContainer, [&] {
        enemies_in_fight = this->getEnemies();
        float enemyHP = (std::ranges::empty(this->enemies) ? 0 : float(this->enemies[enemy_selected].getStat("HP")) / float(this->enemies[enemy_selected].getStat("maxHP")));
        auto enemy_win = window(text("Enemies") | bold,
                                fightContainer->Render());
        auto fight_win = window(text("Fight Info") | bold, vbox({
                color(
                Color::Red,
                gauge(
                    enemyHP
                ) | vcenter | flex
            ),
            separator(),
            hflow(paragraph(this->fightMsg)) | size(HEIGHT, EQUAL, 13) | size(WIDTH, EQUAL, 70) | vcenter,
            separator(),
            color(
                Color::Green,
                gauge(
                    float(this->player.getHP()) / float(this->player.getmaxHP())
                ) | vcenter | flex
            ),
            separator(),
            color(
                Color::Blue,
                gauge(
                    float(this->playerAP) / float(this->player.getSPD(selectedWeapon))
                ) | vcenter | flex
            ),
        }) | size(WIDTH, EQUAL, 60) ) | vcenter | hcenter ;

        return hbox({
            enemy_win | flex,
            fight_win | flex
        }) | hcenter | flex_grow | border;
    });

    /* ********************************
    ** UI
    *  ********************************/

    std::vector<std::string> tabs = { " World ", " Shop ", " Player ", " Menu " };
    auto tab_selection = Toggle(&tabs, &tab_index);
    auto tab_content = Container::Tab( { World, Shop, Player, Menu }, &tab_index );
    auto main_container = Container::Vertical({ tab_selection, tab_content });

    auto main_container_2 = Container::Tab({ main_container, FightPopUp }, &popup_index);

    auto main_renderer = Renderer(main_container_2, [&] {
        Element document = vbox({
            text("Rogue Quest")          | bold | hcenter,
            tab_selection->Render()      | hcenter | border,
            tab_content->Render()        | flex | hcenter
        }) | flex | border;

        if ( popup_index == 1 ) {
            document = dbox({
                document,
                FightPopUp->Render() | clear_under | center,
            });
        }

        return document;
    });

    bool refresh_ui_continue = true;
    std::thread refresh_ui([&] {
        while (refresh_ui_continue) {
            std::this_thread::sleep_for(0.05s);
            this->screen.PostEvent(Event::Custom);
        }
    });

    this->screen.Loop(main_renderer);
    refresh_ui_continue = false;
    refresh_ui.join();
}
