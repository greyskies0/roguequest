#include "headers/world.hpp"

std::vector<std::string> World::getLocationNames() const {
    std::vector<std::string> ret;
    for (auto loc : this->locations) {
        ret.push_back(loc.getName());
    }
    return ret;
}

void World::changeLocation(const int newLocationIndex) {
    this->currLocation_idx = newLocationIndex;
    this->currLocation = this->locations[newLocationIndex];
}

void World::killBoss(const int i) {
    this->defeatedBosses[i] = 1;
}

std::string World::getStory(const int i) const {
    return this->story[i];
}

std::string World::getCurrLocationName() const {
    return this->currLocation.getName();
}

int World::getCurrLocationIndex() const {
    return this->currLocation_idx;
}

void World::loadLocations() {
    namespace fs = std::filesystem;

    std::regex correct_loc_name("^[a-zA-Z ]*$");
    std::regex correct_loc_diff("^\\d*$");
    std::regex correct_loc_enemies("^([a-zA-Z ]*;?)*$");
    std::regex correct_loc_boss("^[a-zA-Z ]* Boss$");

    std::string locName = "";
    unsigned int locDiff = 0;
    std::stringstream enemiesList;
    std::string enemy;
    std::vector<std::string> enemies = {};
    std::string bossName;
    fs::path locFile = fs::current_path() / "assets/locations.txt";
    if ( fs::exists(locFile) ) {
        auto fileContents = FileHandler::fGetLines(locFile.string());
        if ( std::ranges::size(fileContents) % 4 != 0 ) {
            Logger::log("[ERROR] File contains wrong number of entries!\n");
            throw MyException(std::string(__PRETTY_FUNCTION__) + ": Locations are not stored in the correct format!");
            return;
        }
        for (size_t i = 0; i < std::ranges::size(fileContents); i += 4) {
            enemies.clear();
            if ( !std::regex_match(fileContents[i],   correct_loc_name)    ||
                 !std::regex_match(fileContents[i+1], correct_loc_diff)    ||
                 !std::regex_match(fileContents[i+2], correct_loc_enemies) ||
                 !std::regex_match(fileContents[i+3], correct_loc_boss) ) {
                throw MyException(std::string(__PRETTY_FUNCTION__) + ": Locations are not stored in the correct format!");
            }
            locName = fileContents[i];
            locDiff = std::stoi(fileContents[i+1]);
            enemiesList = std::stringstream(fileContents[i+2]);
            while ( std::getline(enemiesList, enemy, ';') ) {
                enemies.push_back(enemy);
            }
            bossName = fileContents[i+3];
            this->locations.push_back(Location(locName, locDiff, enemies, bossName));
        }
    } else {
        Logger::log("[ERROR] Couldn't load locations from the file " + locFile.string() + "\nCheck if the file exists!");
        throw MyException(std::string(__PRETTY_FUNCTION__) + ": Couldn't open the file " + locFile.string() + "!");
    }
    std::vector<std::string> logs;
    logs.emplace_back("Loaded locations:");
    for (auto l : this->locations) {
        logs.push_back(" > " + l.getName());
        for (auto e : l.availableEnemies) {
            logs.push_back("   > " + e);
        }
    }
    Logger::log(logs);
}

World::World() {
    this->loadLocations();
    std::fill(std::ranges::begin(this->defeatedBosses),
              std::ranges::end(this->defeatedBosses),
              false);
    this->currLocation = this->locations[0];
    this->currLocation_idx = 0;
    this->story = FileHandler::fGetLines((fs::current_path() / "assets/story.txt").string());
}

World::~World() = default;
