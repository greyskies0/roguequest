#include "headers/location.hpp"

std::string Location::getName() const {
    return this->name;
}

unsigned int Location::getDifficulty() const {
    return this->difficulty;
}

NPC Location::generateEnemy() const {
    std::string enemyName = Rand::element(this->availableEnemies);
    unsigned int locDifficulty = this->getDifficulty();
    unsigned int enemyBaseStat = 3 * locDifficulty;
    unsigned int enemyHP = 50 + (35 * locDifficulty) + Rand::range(locDifficulty*5);

    std::map<std::string, int> enemyStats {
        {"HP",    enemyHP},
        {"maxHP", enemyHP},
        {"ATK",   enemyBaseStat + (Rand::range(locDifficulty)+1)/2},
        {"DEF",   enemyBaseStat + (Rand::range(locDifficulty)+1)/2},
        {"SPD",   enemyBaseStat + (Rand::range(locDifficulty)+1)/2},
    };

    return NPC(enemyName, enemyStats);
}

NPC Location::generateBoss() const {
    std::string bossName = this->bossName;
    Logger::log(bossName);
    unsigned int locDifficulty = this->getDifficulty();
    unsigned int bossBaseStat = (3 * locDifficulty)   * 2;
    unsigned int bossHP = (50 + (35 * locDifficulty)) * 2;

    std::map<std::string, int> bossStats {
        {"HP",    bossHP},
        {"maxHP", bossHP},
        {"ATK",   bossBaseStat + (Rand::range(locDifficulty)+1)/2},
        {"DEF",   bossBaseStat + (Rand::range(locDifficulty)+1)/2},
        {"SPD",   bossBaseStat + (Rand::range(locDifficulty)+1)/2},
    };

    return NPC(bossName, bossStats);
}

Location::Location()
    : name("location"), difficulty(0), bossName("NO BOSS"), availableEnemies({}) {}

Location::Location(const std::string _name,
                   const unsigned int _diff,
                   const std::vector<std::string> _enems,
                   const std::string _bossName)
    : name(_name), difficulty(_diff), bossName(_bossName), availableEnemies(_enems) {}

Location::~Location() = default;
