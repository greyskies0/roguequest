#include "headers/character.hpp"

/*
** -= NPC CLASS =-
*/
std::string NPC::getName() const {
    return this->name;
}

int NPC::getStat(const std::string _stat) const {
    if ( !this->stats.contains(_stat) ) {
        throw MyException("There is no stat with the name: " + _stat);
    }
    auto searchedStat = this->stats.find(_stat);

    if (searchedStat != std::ranges::end(this->stats)) {
        return searchedStat->second;
    } else throw MyException("There is no stat with the name: " + _stat);
}

void NPC::takeDamage(const unsigned int _incomingDamage) {
    auto HPstat = this->stats.find("HP");

    if (HPstat != std::ranges::end(this->stats)) {
        HPstat->second -= _incomingDamage;
        return;
    } else throw MyException(std::string(__PRETTY_FUNCTION__) + ": NPC has no `HP` stat!");
}

NPC::NPC() {
    this->name = "";
    this->stats = {};
}

NPC::NPC(const std::string _name, const std::map<std::string, int> _stats)
    : name(_name), stats(_stats) {
}

NPC::~NPC() = default;


/*
** -= PLAYER CLASS =-
*/
std::vector<std::string> Player::getWeaponNames() const {
    std::vector<std::string> returned;
    std::string entry;
    for (auto wep : this->weapons) {
        entry = wep.Item::getFullName() + " (" + std::to_string(wep.Item::getStat()) + " ATK | " + std::to_string(wep.getAtkSpeed()) + " SPD)";
        returned.push_back(entry);
    }
    return returned;
}

std::vector<std::string> Player::getArmorNames() const {
    std::vector<std::string> returned;
    std::string entry;
    for (auto arm : this->armor) {
        entry = arm.Item::getFullName() + " (" + std::to_string(arm.Item::getStat()) + " DEF)";
        returned.push_back(entry);
    }
    return returned;
}

std::vector<Weapon> Player::getWeapons() const {
    return this->weapons;
}

std::vector<Armor> Player::getArmor() const {
    return this->armor;
}

int Player::getHP() const {
    auto HPstat = this->stats.find("HP");

    if (HPstat != std::ranges::end(this->stats)) {
        return HPstat->second;
    } else throw MyException(std::string(__PRETTY_FUNCTION__) + ": Player has no `HP` stat!");
}

int Player::getmaxHP() const {
    auto HPstat = this->stats.find("maxHP");

    if (HPstat != std::ranges::end(this->stats)) {
        return HPstat->second;
    } else throw MyException(std::string(__PRETTY_FUNCTION__) + ": Player has no `maxHP` stat!");
}

int Player::getEXP() const {
    auto HPstat = this->stats.find("EXP");

    if (HPstat != std::ranges::end(this->stats)) {
        return HPstat->second;
    } else throw MyException(std::string(__PRETTY_FUNCTION__) + ": Player has no `EXP` stat!");
}

int Player::getATK(const int i) const {
    return this->weapons[i].Item::getStat();
}

int Player::getSPD(const int i) const {
    return this->weapons[i].getAtkSpeed();
}

int Player::getDEF(const int i) const {
    return this->armor[i].Item::getStat();
}

unsigned int Player::getGold() const {
    auto gold = this->stats.find("gold");

    if (gold != std::ranges::end(this->stats)) {
        return gold->second;
    } else throw MyException(std::string(__PRETTY_FUNCTION__) + ": Player has no `gold` stat!");
}

void Player::takeDamage(const unsigned int _incomingDamage) {
    auto HPstat = this->stats.find("HP");

    if (HPstat != std::ranges::end(this->stats)) {
        HPstat->second -= _incomingDamage;
        return;
    } else throw MyException(std::string(__PRETTY_FUNCTION__) + ": Player has no `HP` stat!");
}

void Player::addGold(const int _gold) {
    this->stats["gold"] += _gold;
    return;
}

void Player::addEXP(const int _xp) {
    this->stats["EXP"] += _xp;
    return;
}

void Player::setHP() {
    this->stats["HP"] = 10;
    return;
}

void Player::setHP(const int _hp) {
    this->stats["HP"] = (this->stats["maxHP"] < _hp ? this->stats["maxHP"] : _hp);
    return;
}

void Player::tryLevelUp() {
    int playerEXP = this->stats["EXP"];
    int increase = (playerEXP / 100) * 25;
    this->stats["maxHP"] = 100 + increase;
    return;
}

void Player::rest() {
    if (this->stats["HP"] != this->stats["maxHP"]) {
        this->stats["EXP"] = .8 * this->stats["EXP"];
        this->tryLevelUp();
        this->stats["HP"] = this->stats["maxHP"];
    }
    return;
}

void Player::restInTavern() {
    if (this->stats["HP"] != this->stats["maxHP"]) {
        this->stats["HP"] = this->stats["maxHP"];
        this->addGold(-300);
    }
    return;
}

void Player::newWeapon(const Weapon wep) {
    this->addGold(-wep.getCost());
    this->weapons.push_back(wep);
    return;
}

void Player::newArmor(const Armor arm) {
    this->addGold(-arm.getCost());
    this->armor.push_back(arm);
    return;
}

Player& Player::operator=(const Player& p) {
    this->stats = p.stats;
    this->weapons = p.weapons;
    this->armor = p.armor;

    return *this;
}

Player::Player() {
    Weapon wep1("Sword", 4, 4, "Old", 0);
    Weapon wep2("Slingshot", 3, 5, "Old", 0);
    Armor arm("Clothes", 6, "Worn", 0);

    this->stats["maxHP"] = 100;
    this->stats["HP"] = 100;
    this->stats["EXP"] = 0;
    this->stats["gold"] = 200;
    this->weapons.emplace_back(wep1);
    this->weapons.push_back(wep2);
    this->armor.emplace_back(arm);
}

Player::Player(
    const std::map<std::string, int> s,
    const std::vector<Weapon> w,
    const std::vector<Armor> a
) : stats(s), weapons(w), armor(a) {}

Player::~Player() = default;
