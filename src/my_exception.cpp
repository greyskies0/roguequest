#include "headers/my_exception.hpp"

char * MyException::what() {
    return this->msg;
}

MyException::MyException(char * _msg) : msg(_msg) { }
MyException::MyException(std::string _msg) : msg(_msg.data()) { }
