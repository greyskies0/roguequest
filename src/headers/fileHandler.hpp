#ifndef FILEHANDLER_HPP
#define FILEHANDLER_HPP

#include <fstream>
#include <string>
#include <vector>
#include <iostream>

#include "logger.hpp"
#include "my_exception.hpp"

class FileHandler
{
    public:
        static std::vector<std::string> fGetLines(std::string fname) {
            std::vector<std::string> out;
            std::string line;
            std::ifstream inFile;
            Logger::log("Opening " + fname);
            inFile.open(fname);
            if (inFile) {
                while (std::getline(inFile, line)) {
                    out.push_back(line);
                }
            } else {
                Logger::log("[ERROR] Couldn't open the file " + fname + "!");
                throw MyException(std::string(__PRETTY_FUNCTION__) + ": Couldn't open the file " + fname + "!");
            }
            return out;
        }
};

#endif // FILEHANDLER_HPP
