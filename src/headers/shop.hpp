#ifndef SHOP_HPP_
#define SHOP_HPP_

#include <vector>
#include <array>
#include <regex>
#include <ranges>

#include "item.hpp"
#include "random.hpp"
#include "fileHandler.hpp"
#include "logger.hpp"
#include "my_exception.hpp"

class Shop
{
    private:
        std::vector<Weapon> weapons;
        std::vector<Armor>  armor;

    public:
        std::vector<std::string> getWeaponNames() const;
        std::vector<std::string> getArmorNames()  const;

        void loadWeapons(const size_t i);
        void loadArmor(const size_t i);

        Weapon buyWeapon(const size_t i);
        Armor  buyArmor(const size_t i);

        std::string getWepInfo(const size_t i) const;
        std::string getArmInfo(const size_t i) const;

        unsigned int getWepCost(const size_t i) const;
        unsigned int getArmCost(const size_t i) const;

        Shop();
        ~Shop() = default;
};

#endif // SHOP_HPP_
