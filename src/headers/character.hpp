#ifndef CHARACTER_HPP_
#define CHARACTER_HPP_

#include <string>
#include <map>
#include <vector>
#include <math.h>

#include "item.hpp"
#include "logger.hpp"
#include "my_exception.hpp"

class NPC
{
    protected:
        std::string name;

        std::map<std::string, int> stats {
            {"HP",    0},
            {"maxHP", 0},
            {"ATK",   0},
            {"DEF",   0},
            {"SPD",   0}
        };

    public:
        std::string getName() const;
        int getStat(const std::string _stat) const;

        void takeDamage(const unsigned int _incomingDamage);

        NPC();
        NPC(const std::string _name, const std::map<std::string, int> _stats);
        ~NPC();
};

class Player
{
    private:
        std::map<std::string, int> stats {
            {"HP",    0},
            {"maxHP", 0},
            {"EXP",   0},
            {"gold",  0},
        };
        std::vector<Weapon> weapons;
        std::vector<Armor>  armor;

    public:
        std::vector<std::string> getWeaponNames() const;
        std::vector<std::string> getArmorNames() const;
        std::vector<Weapon> getWeapons() const;
        std::vector<Armor> getArmor() const;
        int getHP() const;
        int getmaxHP() const;
        int getEXP() const;
        int getATK(const int i) const;
        int getSPD(const int i) const;
        int getDEF(const int i) const;
        unsigned int getGold() const;

        void takeDamage(const unsigned int _incomingDamage);
        void addGold(const int _gold);
        void addEXP(const int _xp);
        void setHP();
        void setHP(const int _hp);

        void tryLevelUp();
        void rest();
        void restInTavern();

        void newWeapon(const Weapon wep);
        void newArmor(const Armor arm);

        Player& operator=(const Player& p);

        Player();
        Player(std::map<std::string, int> s, std::vector<Weapon> w, std::vector<Armor> a);
        ~Player();
};

#endif // CHARACTER_HPP_
