#ifndef WORLD_HPP_
#define WORLD_HPP_

#include <filesystem>
#include <sstream>
#include <array>
#include <algorithm>
#include <regex>
#include <ranges>

#include "location.hpp"
#include "fileHandler.hpp"
#include "logger.hpp"
#include "my_exception.hpp"

#define LOC_NUM 4

class World
{
    private:
        int currLocation_idx = 0;
        std::vector<std::string> story;

    public:
        std::vector<Location> locations;
        Location currLocation;
        std::array<bool, LOC_NUM> defeatedBosses;

        std::vector<std::string> getLocationNames() const;
        std::string getCurrLocationName() const;
        int getCurrLocationIndex() const;
        void changeLocation(const int newLocationIndex);

        void killBoss(const int i);
        std::string getStory(const int i) const;

        void loadLocations();

        World();
        ~World();
};

#endif // WORLD_HPP_
