#ifndef GAME_HPP_
#define GAME_HPP_

#include <ftxui/dom/elements.hpp>
#include <ftxui/dom/node.hpp>
#include <ftxui/screen/screen.hpp>
#include <ftxui/component/screen_interactive.hpp>
#include <ftxui/component/captured_mouse.hpp>
#include <ftxui/component/component.hpp>
#include <ftxui/component/component_base.hpp>
#include <ftxui/component/component_options.hpp>

#include <thread>
#include <mutex>
#include <semaphore>
#include <regex>
#include <ranges>

#include "world.hpp"
#include "shop.hpp"
#include "logger.hpp"

#define DMG_MULTIPLIER 2

class Game
{
    private:
        std::mutex m;
        std::vector<std::string> SunTzu {
            "It's not the size of the dog in the fight, it's the size of the fight in the dog",
            "To win any battle, you must fight as if you are already dead",
            "The only reason a warrior is alive is to fight, and the only reason a warrior fights is to win",
            "The supreme art of war is to subdue the enemy without fighting",
            "Let your plans be dark and impenetrable as night, and when you move, fall like a thunderbolt",
            "In the midst of chaos, there is also opportunity",
            "Victorious warriors win first and then go to war, while defeated warriors go to war first and then seek to win",
            "Even the finest sword plunged into salt water will eventually rust",
            "Move swift as the Wind and closely-formed as the Wood. Attack like the Fire and be still as the Mountain",
            "When the enemy is relaxed, make them toil. When full, starve them. When settled, make them move",
            "Who wishes to fight must first count the cost",
            "Attack is the secret of defense; defense is the planning of an attack",
        };
        enum fightType {
            normal,
            boss,
            quest
        };
        std::string welcome_msg = "You are a lone traveller wandering through the world looking for adventures. Suddenly, you have found yourself in a land which is not described by any map known to you. Curious of this new discovery, you decide to examine what awaits there...";
        std::string title = R"(
                '||''|.                                        ..|''||                             .
                 ||   ||    ...     ... . ... ...    ....     .|'    ||  ... ...    ....   ....  .||.
                 ||''|'   .|  '|.  || ||   ||  ||  .|...||    ||      ||  ||  ||  .|...|| ||. '   ||
                 ||   |.  ||   ||   |''    ||  ||  ||         '|.  '. '|  ||  ||  ||      . '|..  ||
                .||.  '|'  '|..|'  '||||.  '|..'|.  '|...'      '|...'|.  '|..'|.  '|...' |'..|'  '|.'
                                  .|....'
)";
        ftxui::ScreenInteractive screen = ftxui::ScreenInteractive::TerminalOutput();
        World world = World();
        Player player = Player();

        Shop shop = Shop();
        std::vector<std::string> s_weapons;
        std::vector<std::string> s_armor;

        int selectedWeapon = 0;
        int selectedArmor = 0;
        int playerAP = 0;
        int popup_index = 0;

        std::string eventMessage;
        std::string fightMsg;
        std::atomic<bool> in_fight;
        std::vector<NPC> enemies;
        std::vector<std::string> getEnemies() const;

        void fight(const fightType type);
        void attackEnemy(const size_t i);
        void attackPlayer();
        void flee();

        void rest();
        void restInTavern();
        void travel(const int newLocationIndex);

        void newGame();
        void save(const std::string sName);
        void load(const std::string sName);

        void updateShop();

public:
        void start();
        void showTitle() const;

        Game();
        ~Game();
};

#endif // GAME_HPP_
