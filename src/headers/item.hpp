#ifndef ITEM_HPP_
#define ITEM_HPP_

#include <string>

class Item
{
    protected:
        std::string name;
        unsigned int stat;
        unsigned int cost;
        std::string modifier;
        int bonus;

    public:
        std::string getName()     const;
        std::string getModifier() const;
        std::string getFullName() const;
        unsigned int getStat()    const;
        unsigned int getCost()    const;
        int getBonus()            const;

        Item(const std::string _name, const unsigned int _stat);
        Item(const std::string _name,
             const unsigned int _stat,
             const std::string _modifier,
             const int _bonus);
        virtual ~Item();
};

class Weapon : public Item
{
    private:
        unsigned int atkSpeed;

    public:
        unsigned int getAtkSpeed() const;

        Weapon(const std::string _name, const unsigned int _stat, const unsigned int _atkSpeed);
        Weapon(const std::string _name,
               const unsigned int _stat,
               const unsigned int _atkSpeed,
               const std::string _modifier,
               const int _bonus);
        ~Weapon();
};

class Armor : public Item
{
    public:
        Armor(const std::string _name, const unsigned int _stat);
        Armor(const std::string _name,
              const unsigned int _stat,
              const std::string _modifier,
              const int _bonus);
        ~Armor();
};

#endif // ITEM_HPP_
