#ifndef LOCATION_HPP_
#define LOCATION_HPP_

#include "character.hpp"
#include "random.hpp"
#include "logger.hpp"

#include <iostream>

class Location
{
    private:
        std::string name;
        unsigned int difficulty;
        std::string bossName;

    public:
        std::vector<std::string> availableEnemies;
        std::string getName() const;
        unsigned int getDifficulty() const;
        NPC generateEnemy() const;
        NPC generateBoss() const;

        Location();
        Location(const std::string _name,
                 const unsigned int _diff,
                 const std::vector<std::string> _enems,
                 const std::string _bossName);
        ~Location();
};

#endif // LOCATION_HPP_
