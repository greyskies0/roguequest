#ifndef MY_EXCEPTION_HPP_
#define MY_EXCEPTION_HPP_

#include <exception>
#include <string>

class MyException : public std::exception
{
    private:
        char * msg;
    public:
        char *what();
        MyException(char * _msg);
        MyException(std::string _msg);
};

#endif // MY_EXCEPTION_HPP_
