#ifndef LOGGER_HPP_
#define LOGGER_HPP_

#include <fstream>
#include <string>
#include <filesystem>
#include <vector>

namespace fs = std::filesystem;
const fs::path fpath = fs::current_path() / "log.txt";

class Logger {
    private:
        static constexpr auto date = [] {
            typedef std::chrono::system_clock clk;
            time_t now = clk::to_time_t(clk::now());
            return std::put_time(localtime(&now), "[%T %d-%m-%Y]");
        };


    public:
        static void log(std::string msg) {
            std::ofstream logFile;
            logFile.open(fpath, std::ios_base::app);
            logFile << date() << "\n  " << msg << std::endl;
        }
        static void log(std::vector<std::string> msg) {
            std::ofstream logFile;
            logFile.open(fpath, std::ios_base::app);
            logFile << date() << std::endl;
            for (auto s : msg) {
                logFile << "  " << s << std::endl;
            }
        }
};

#endif // LOGGER_HPP_
