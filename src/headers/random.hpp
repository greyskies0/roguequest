#ifndef RANDOM_HPP_
#define RANDOM_HPP_

#include <vector>
#include <string>
#include <random>
#include <iostream>
#include <ranges>

class Rand {
    public:
        static std::string element(const std::vector<std::string> vec) {
            int top = std::ranges::size(vec) - 1;

            std::random_device dev;
            std::mt19937 rng(dev());
            std::uniform_int_distribution<std::mt19937::result_type> distribution(0, top);

            int pos = distribution(rng);

            return vec[pos];
        }
        static std::string element_poisson(const std::vector<std::string> vec, const size_t most_f = 0) {
            std::random_device dev;
            std::mt19937 rng(dev());
            std::poisson_distribution<std::mt19937::result_type> distribution(most_f);

            size_t pos = distribution(rng);
            pos = (pos >= std::ranges::size(vec) ? std::ranges::size(vec) - 1 : pos);

            return vec[pos];
        }
        static std::vector<std::string> n_elements_poisson(std::vector<std::string> vec, size_t n, const size_t most_f = 0) {
            std::vector<std::string> out;
            n = (n > std::ranges::size(vec) ? std::ranges::size(vec) : n);
            for (size_t i = 0; i < n; ++i) {
                std::random_device dev;
                std::mt19937 rng(dev());
                std::poisson_distribution<std::mt19937::result_type> distribution(most_f);
                size_t pos = distribution(rng);
                pos = (pos >= std::ranges::size(vec) ? std::ranges::size(vec) - 1 : pos);
                out.push_back(vec[pos]);
                vec.erase(std::ranges::begin(vec) + pos);
            }

            return out;
        }
        static int percent() {
            std::random_device dev;
            std::mt19937 rng(dev());
            std::uniform_int_distribution<std::mt19937::result_type> distribution(0, 100);

            return distribution(rng);
        }
        static int range(const int _range) {
            std::random_device dev;
            std::mt19937 rng(dev());
            std::uniform_int_distribution<std::mt19937::result_type> distribution(-_range, _range);

            return distribution(rng);
        }
        static int number(const int _cap) {
            std::random_device dev;
            std::mt19937 rng(dev());
            std::uniform_int_distribution<std::mt19937::result_type> distribution(0, _cap);

            return distribution(rng);
        }
};

#endif // RANDOM_HPP_
