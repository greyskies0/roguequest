#include "headers/game.hpp"

std::vector<std::string> Game::getEnemies() const {
    if ( std::ranges::empty(this->enemies) ) {
        return { " --- " };
    }
    std::vector<std::string> out;

    for ( const auto& enem : this->enemies ) {
        out.push_back(enem.getName() + " (" + std::to_string(enem.getStat("HP")) + ")");
    }

    return out;
}

void Game::fight(const fightType type) {
    try {
        if ( this->player.getHP() <= 0 ) {
            return;
        }
        m.lock();
        this->enemies.clear();
        this->in_fight = false;
        this->fightMsg = Rand::element(this->SunTzu);
        this->playerAP = this->player.getSPD(this->selectedWeapon);

        if ( type == fightType::normal ) {
            for ( int i = 0; i < 1 + Rand::number(this->world.getCurrLocationIndex() * 2); ++i ) {
                this->enemies.push_back( this->world.currLocation.generateEnemy() );
            }
        } else if ( type == fightType::boss ) {
            this->enemies.push_back( this->world.currLocation.generateBoss() );
        } else {
            this->enemies.push_back( this->world.currLocation.generateBoss() );
        }
        this->popup_index = 1; // enter fighting tab
        m.unlock();
        return;
    } catch (MyException &e) {
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (std::exception e) {
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (...) {
        this->screen.ExitLoopClosure();
        return;
    }
}

void Game::flee() {
    try {
        using namespace std::chrono_literals;
        if ( this->in_fight ) {
            return;
        } else if ( std::ranges::empty(this->enemies) ) {
            this->fightMsg = "Press 'END TURN' to exit the fight";
            return;
        }

        auto flee_thread = std::thread([&] {
            int chance    = Rand::percent();
            int HP        = this->player.getHP();
            int maxHP     = this->player.getmaxHP();
            int HPpercent = (HP * 100) / maxHP;
            this->fightMsg = "Chance of fleeing: " + std::to_string(HPpercent) + '%';
            std::this_thread::sleep_for(2s);
            if ( chance < HPpercent ) {
                this->fightMsg = "Success!";
                std::this_thread::sleep_for(2s);
                this->popup_index = 0;
                this->enemies.clear();
                this->in_fight = false;
            } else {
                this->fightMsg = "You have failed to flee the battle! ";
                std::this_thread::sleep_for(2s);
                this->attackPlayer();
            }
        });
        flee_thread.detach();
    } catch (MyException &e) {
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (std::exception e) {
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (...) {
        this->screen.ExitLoopClosure();
        return;
    }
}

void Game::attackEnemy(const size_t i) {
    try {
        if ( this->in_fight || this->player.getHP() <= 0 ) {
            return;
        }

        bool is_boss = ( this->enemies[i].getName().find("Boss") != std::string::npos );

        if ( this->playerAP > 0 && !std::ranges::empty(this->enemies) && this->player.getHP() > 0 ) {
            auto ATK  = this->player.getATK(this->selectedWeapon);
            auto DEF  = this->enemies[i].getStat("DEF");
            auto DMG  = (( ATK + (ATK / DEF) ) * DMG_MULTIPLIER) + Rand::range(2);
            auto name = this->enemies[i].getName();
            try {
                this->enemies[i].takeDamage(DMG);
            } catch (MyException &e) {
                this->screen.ExitLoopClosure();
                std::cout << e.what() << '\n';
                return;
            }
            this->fightMsg = name + " has taken " + std::to_string(DMG) + " damage!";
            this->playerAP -= 1;
            if ( this->enemies[i].getStat("HP") <= 0 ) {
                this->enemies.erase(std::ranges::cbegin(this->enemies) + i);
                const unsigned int diff    = this->world.currLocation.getDifficulty();
                const unsigned int currEXP = this->player.getEXP();
                const double penalty       = double( diff * 500 < currEXP ? currEXP / 200 : 1 );
                int gainedGold             = 5 * diff * (10 + Rand::range(3));
                gainedGold                 = ( is_boss ? gainedGold * 6 : gainedGold );
                int gainedEXP              = 3 * diff * (20 + Rand::range(2));
                gainedEXP                  = ( is_boss ? gainedEXP  * 6 : gainedEXP ) / penalty;
                this->player.addGold(gainedGold);
                this->player.addEXP(gainedEXP);
                this->player.tryLevelUp();
                if ( std::ranges::empty(this->enemies) ) {
                    this->fightMsg += " You have won the battle! Rewards: " + std::to_string(gainedGold) + "g, " + std::to_string(gainedEXP) + "xp";
                    this->in_fight = false;
                    if ( is_boss ) {
                        this->world.killBoss(this->world.getCurrLocationIndex());
                        this->eventMessage = this->world.getStory(this->world.getCurrLocationIndex());
                        this->updateShop();
                    }
                }
            }
        } else if ( std::ranges::empty(this->enemies) ) {
            this->fightMsg = "Press 'END TURN' to exit the fight";
        } else if ( this->playerAP <= 0 ) {
            this->fightMsg = "You don't have any Action Points left!";
        }
    } catch (MyException &e) {
        this->popup_index = 0;
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (std::exception e) {
        this->popup_index = 0;
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (...) {
        this->popup_index = 0;
        this->screen.ExitLoopClosure();
        return;
    }
}

void Game::attackPlayer() {
    using namespace std::chrono_literals;
    try {
        if ( this->in_fight ) {
            return;
        }
        this->in_fight = true;
        if ( std::ranges::empty(this->enemies) ) {
            this->in_fight = false;
            this->popup_index = 0;
            return;
        }
        auto fight_thread = std::thread([&] {
            for ( const auto& e : enemies ) {
                auto ATK = e.getStat("ATK");
                auto DEF = this->player.getDEF(this->selectedArmor);
                auto DMG = ( ATK + (ATK/DEF) ) * DMG_MULTIPLIER;
                auto DMG_total = DMG;
                for ( int i = 0; i < e.getStat("SPD"); ++i ) {
                    DMG_total = DMG + Rand::range(2);
                    this->player.takeDamage(DMG_total);
                    this->fightMsg = "You have taken "
                        + std::to_string(DMG_total)
                        + " damage from "
                        + e.getName() + "!";
                    std::this_thread::sleep_for(1s);
                    if ( this->player.getHP() <= 0 ) {
                        break;
                    }
                }
            }
            this->fightMsg += " *END OF ENEMY TURN*";
            this->playerAP = this->player.getSPD(this->selectedWeapon);
            this->in_fight = false;
            if ( this->player.getHP() <= 0 ) {
                this->fightMsg = "You have lost the battle!";
                this->player.addGold( -(this->player.getGold() / 5) );
                this->player.addEXP ( -(this->player.getEXP()  / 5) );
                std::this_thread::sleep_for(2s);
                this->popup_index = 0;
                return;
            }
        });
        fight_thread.detach();
    } catch (MyException &e) {
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (std::exception e) {
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (...) {
        this->screen.ExitLoopClosure();
        return;
    }
}

void Game::rest() {
    using namespace std::chrono_literals;
    std::thread th([&]{
        m.lock();
        std::this_thread::sleep_for(1s);
        this->eventMessage = "Due to sleeping in the wild your ability to fight has reduced...";
        this->player.rest();
        m.unlock();
    });
    th.detach();
    return;
}

void Game::restInTavern() {
    using namespace std::chrono_literals;
    std::thread th([&]{
        m.lock();
        std::this_thread::sleep_for(1s);
        if (this->player.getGold() >= 300) {
            this->eventMessage = "Sleeping in a tavern has cost you 300 Gold";
            this->player.restInTavern();
        } else {
            this->eventMessage = "You can't afford 300 Gold to sleep in a tavern!";
        }
        m.unlock();
    });
    th.detach();
    return;
}

void Game::travel(const int newLocationIndex) {
    using namespace std::chrono_literals;
    try {
        m.lock();
        this->world.changeLocation(newLocationIndex);
        m.unlock();
        // auto travel_thread = std::thread([&] {
        //     for ( size_t i = 0; i < 3; ++i ) {
        //         std::this_thread::sleep_for(1s);
        //         this->eventMessage += ".";
        //     }
        //     this->eventMessage = "You have arrived at the new location!";
        //     this->world.changeLocation(newLocationIndex);
        //     return;
        // });
        // travel_thread.detach();
        return;
    } catch (MyException &e) {
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (std::exception e) {
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (...) {
        this->screen.ExitLoopClosure();
        return;
    }
}

void Game::showTitle() const {
    using namespace std::chrono_literals;
    std::string str = "";
    std::cout << "Press ENTER to begin...\n";
    std::getline(std::cin, str);
    for (auto c : this->title) {
        std::cout << c << std::flush;
        std::this_thread::sleep_for(0.005 * 1s);
    }
    std::this_thread::sleep_for(2s);
}

void Game::newGame() {
    try {
        this->load("*new*");
        return;
    } catch (MyException &e) {
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (std::exception e) {
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (...) {
        this->screen.ExitLoopClosure();
        return;
    }
}

void Game::save(const std::string sName) {
    namespace fs = std::filesystem;
    try {
        m.lock();
        if (sName == "*new*") {
            this->eventMessage = "You can't save into the *new* slot!";
            return;
        }
        fs::path save_d = fs::current_path() / ("saves/" + sName);
        std::ofstream player_f;
        std::ofstream world_f;
        std::ofstream weapons_f;
        std::ofstream armor_f;

        if (!fs::exists(save_d)) {
            fs::create_directory(save_d);
        }

        player_f.open((save_d / "player").string());
        if (player_f) {
            player_f << this->player.getHP()    << '\n'
                     << this->player.getmaxHP() << '\n'
                     << this->player.getEXP()   << '\n'
                     << this->player.getGold()  << '\n';
        } else {
            Logger::log("[ERROR] Couldn't create the Player save file for " + save_d.string());
            throw MyException(std::string(__PRETTY_FUNCTION__) + ": Couldn't create the Player save file!");
        }

        world_f.open((save_d / "world").string());
        if (world_f) {
            world_f << this->world.getCurrLocationIndex() << '\n';
            for (const auto &b : this->world.defeatedBosses) {
                world_f << b << '\n';
            }
        } else {
            Logger::log("[ERROR] Couldn't create the World save file for " + save_d.string());
            throw MyException(std::string(__PRETTY_FUNCTION__) + ": Couldn't create the World save file!");
        }

        weapons_f.open((save_d / "weapons").string());
        if (weapons_f) {
            for (const auto &w : this->player.getWeapons()) {
                weapons_f << w.getName()                << ';'
                          << w.getStat() - w.getBonus() << ';'
                          << w.getAtkSpeed()            << ';'
                          << w.getModifier()            << ';'
                          << w.getBonus()               << '\n';
            }
        } else {
            Logger::log("[ERROR] Couldn't create the Weapons save file for " + save_d.string());
            throw MyException(std::string(__PRETTY_FUNCTION__) + ": Couldn't create the Weapons save file!");
        }

        armor_f.open((save_d / "armor").string());
        if (armor_f) {
            for (const auto &a : this->player.getArmor()) {
                armor_f << a.getName()                << ';'
                        << a.getStat() - a.getBonus() << ';'
                        << a.getModifier()            << ';'
                        << a.getBonus()               << '\n';
            }
        } else {
            Logger::log("[ERROR] Couldn't create the Armor save file for " + save_d.string());
            throw MyException(std::string(__PRETTY_FUNCTION__) + ": Couldn't create the Armor save file!");
        }

        this->eventMessage = "Saving to " + save_d.filename().string() + ": successful";
        Logger::log("Saving in " + save_d.filename().string() + " successful");
        m.unlock();
    } catch (MyException &e) {
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (std::exception e) {
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (...) {
        this->screen.ExitLoopClosure();
        return;
    }
}

void Game::load(const std::string sName) {
    namespace fs = std::filesystem;
    try {
        m.lock();
        std::regex correct_armor_format("^([a-zA-Z -]+;\\d+;?){2}$");
        std::regex correct_weapon_format("^([a-zA-Z -]+(;\\d+)+;?)([a-zA-Z ]+(;\\d+);?)$");
        std::regex correct_player_format("^\\d+$");
        std::regex correct_world_format("^\\d$");

        fs::path save_d = fs::current_path() / ("saves/" + sName);

        if (!fs::exists(save_d)) {
            Logger::log("[ERROR] Couldn't open the save file: " + save_d.string());
            throw MyException(std::string(__PRETTY_FUNCTION__) + ": Couldn't open the save file!");
        }

        std::map<std::string, int> stats;
        const auto stats_f = FileHandler::fGetLines((save_d / "player").string());
        if ( std::ranges::size(stats_f) != 4 ) {
            Logger::log("[ERROR] save file: " + save_d.string() + " contains wrong number of entries! (" + std::to_string(std::ranges::size(stats_f)) + ", expected 4)");
            throw MyException(std::string(__PRETTY_FUNCTION__) + ": Player save file contains wrong number of entries! (" + std::to_string(std::ranges::size(stats_f)) + ", expected 4)");
        }
        for ( const auto &s : stats_f ) {
            if ( !std::regex_match(s, correct_player_format) ) {
                throw MyException(std::string(__PRETTY_FUNCTION__) + ": Player save file is in the wrong format!");
            }
        }
        const auto HP    = stoi(stats_f[0]);
        const auto maxHP = stoi(stats_f[1]);
        const auto EXP   = stoi(stats_f[2]);
        const auto gold  = stoi(stats_f[3]);
        stats["maxHP"] = maxHP;
        stats["HP"]    = HP;
        stats["EXP"]   = EXP;
        stats["gold"]  = gold;

        const auto world_f = FileHandler::fGetLines((save_d / "world").string());
        if (std::ranges::size(world_f) != 5) {
            Logger::log("[ERROR] save file: " + save_d.string() + " contains wrong number of entries! (" + std::to_string(std::ranges::size(world_f)) + ", expected 5)");
            throw MyException(std::string(__PRETTY_FUNCTION__) + ": World save file contains wrong number of entries! (" + std::to_string(std::ranges::size(stats_f)) + ", expected 5");
        }
        for ( const auto &w : world_f ) {
            if ( !std::regex_match(w, correct_world_format) ) {
                throw MyException(std::string(__PRETTY_FUNCTION__) + ": World save file is in the wrong format!");
            }
        }
        const auto loc_idx = stoi(world_f[0]);
        std::string line;
        std::array<bool, LOC_NUM> defBosses = {
        bool(stoi(world_f[1])),
        bool(stoi(world_f[2])),
        bool(stoi(world_f[3])),
        bool(stoi(world_f[4])),
    };

        std::stringstream weapon_unparsed;
        std::string _wep[5];
        std::vector<Weapon> weapons;
        const auto weapons_f = FileHandler::fGetLines((save_d / "weapons").string());
        for (const auto &wep : weapons_f) {
            if ( !std::regex_match(wep, correct_weapon_format) ) {
                throw MyException(std::string(__PRETTY_FUNCTION__) + ": Weapons save file is in the wrong format!");
            }
            weapon_unparsed = std::stringstream(wep);
            std::getline( weapon_unparsed, _wep[0], ';' ); // name
            std::getline( weapon_unparsed, _wep[1], ';' ); // ATK
            std::getline( weapon_unparsed, _wep[2], ';' ); // SPD
            std::getline( weapon_unparsed, _wep[3], ';' ); // modifier
            std::getline( weapon_unparsed, _wep[4], ';' ); // bonus
            weapons.push_back( Weapon(_wep[0], stoi(_wep[1]), stoi(_wep[2]), _wep[3], stoi(_wep[4])) );
        }

        std::stringstream armor_unparsed;
        std::string _arm[4];
        std::vector<Armor> armor;
        const auto armor_f = FileHandler::fGetLines((save_d / "armor").string());
        for (const auto &arm : armor_f) {
            if ( !std::regex_match(arm, correct_armor_format) ) {
                throw MyException(std::string(__PRETTY_FUNCTION__) + ": Armor save file is in the wrong format!");
            }
            armor_unparsed = std::stringstream(arm);
            std::getline( armor_unparsed, _arm[0], ';' ); // name
            std::getline( armor_unparsed, _arm[1], ';' ); // DEF
            std::getline( armor_unparsed, _arm[2], ';' ); // modifier
            std::getline( armor_unparsed, _arm[3], ';' ); // bonus
            armor.push_back( Armor(_arm[0], stoi(_arm[1]), _arm[2], stoi(_arm[3])) );
        }

        this->player = Player(stats, weapons, armor);
        this->world.defeatedBosses = defBosses;
        this->world.changeLocation(loc_idx);

        if (sName != "*new*") {
            this->eventMessage = "Loading from " + save_d.filename().string() + ": successful";
        } else {
            this->eventMessage = this->welcome_msg;
        }
        Logger::log("Loading from " + save_d.filename().string() + " successful");
        m.unlock();
    } catch (MyException &e) {
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (std::exception e) {
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (...) {
        this->screen.ExitLoopClosure();
        return;
    }
}

void Game::updateShop() {
    try {
        m.lock();
        auto idx = ( (this->world.getCurrLocationIndex() * 3) + 4 );
        this->shop.loadWeapons((idx * 3) + 1);
        this->shop.loadArmor((idx * 3) + 1);
        this->s_weapons = this->shop.getWeaponNames();
        this->s_armor = this->shop.getArmorNames();
        m.unlock();
    } catch (MyException &e) {
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (std::exception e) {
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (...) {
        this->screen.ExitLoopClosure();
        return;
    }
}

Game::Game() {
    try {
        m.unlock();
        const auto logPath = (fs::current_path() / "log.txt");
        fs::remove(logPath);

        this->selectedWeapon = 0;
        this->selectedArmor = 0;
        this->eventMessage = "";
        this->in_fight = false;
    } catch (MyException &e) {
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (std::exception e) {
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
    } catch (...) {
        this->screen.ExitLoopClosure();
        return;
    }
}

Game::~Game() = default;
