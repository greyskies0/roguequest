#include "headers/shop.hpp"

std::vector<std::string> Shop::getWeaponNames() const {
    std::vector<std::string> returned;
    for (auto wep : this->weapons) {
        returned.push_back(wep.Item::getFullName());
    }
    return returned;
}

std::vector<std::string> Shop::getArmorNames() const {
    std::vector<std::string> returned;
    for (auto arm : this->armor) {
        returned.push_back(arm.Item::getFullName());
    }
    return returned;
}

void Shop::loadWeapons(const size_t i) {
    this->weapons.clear();

    auto fContents = FileHandler::fGetLines("./assets/weapons.txt");
    const size_t n = Rand::number(3) + 2;
    auto selection = Rand::n_elements_poisson(fContents, n, i);
    auto modifiers = FileHandler::fGetLines("./assets/modifiers.txt");

    std::regex correct_weapon_format("^[a-zA-Z -]+(;\\d+){2}$");
    std::regex correct_modifier_format("^[a-zA-Z -]+(;-?\\d+)$");

    for ( const auto &w : fContents ) {
        if ( !std::regex_match(w, correct_weapon_format) ) {
            throw MyException(std::string(__PRETTY_FUNCTION__) + ": Weapon " + w + " is stored in the wrong format!");
        }
    }
    for ( const auto &m : modifiers ) {
        if ( !std::regex_match(m, correct_modifier_format) ) {
            throw MyException(std::string(__PRETTY_FUNCTION__) + ": Modifier " + m + " is stored in the wrong format!");
        }
    }

    std::vector<std::string> modSelection;
    std::vector<std::string> bonusSelection;
    std::string modName, bonus;
    for (size_t i = 0; i < n; ++i) {
        auto randElem = std::stringstream(Rand::element(modifiers));
        std::getline( randElem, modName, ';' );
        std::getline( randElem, bonus,   ';' );
        modSelection.push_back(modName);
        bonusSelection.push_back(bonus);
    }
    std::stringstream weapon_unparsed;
    std::string _wep[5];
    std::vector<Weapon> weps;
    for (const auto &wep : selection) {
        weapon_unparsed = std::stringstream(wep);
        std::getline( weapon_unparsed, _wep[0], ';' ); // name
        std::getline( weapon_unparsed, _wep[1], ';' ); // ATK
        std::getline( weapon_unparsed, _wep[2], ';' ); // SPD
        _wep[3] = modSelection.back();
        _wep[4] = bonusSelection.back();
        modSelection.pop_back();
        bonusSelection.pop_back();
            weps.push_back( Weapon(_wep[0], stoi(_wep[1]), stoi(_wep[2]), _wep[3], stoi(_wep[4])) );
        }
        this->weapons = weps;
}

void Shop::loadArmor(const size_t i) {
    this->armor.clear();

    auto fContents = FileHandler::fGetLines("./assets/armor.txt");
    const size_t n = Rand::number(3) + 2;
    auto selection = Rand::n_elements_poisson(fContents, n, i);
    auto modifiers = FileHandler::fGetLines("./assets/modifiers.txt");

    std::regex correct_armor_format("^[a-zA-Z ]+(;\\d+)$");
    std::regex correct_modifier_format("^[a-zA-Z ]+(;-?\\d+)$");

    for ( const auto &a : fContents ) {
        if ( !std::regex_match(a, correct_armor_format) ) {
            throw MyException(std::string(__PRETTY_FUNCTION__) + ": Armor " + a + " is stored in the wrong format!");
        }
    }
    for ( const auto &m : modifiers ) {
        if ( !std::regex_match(m, correct_modifier_format) ) {
            throw MyException(std::string(__PRETTY_FUNCTION__) + ": Modifier " + m + " is stored in the wrong format!");
        }
    }

    std::vector<std::string> modSelection;
    std::vector<std::string> bonusSelection;
    std::string modName, bonus;
    for (size_t i = 0; i < n; ++i) {
        auto randElem = std::stringstream(Rand::element(modifiers));
        std::getline( randElem, modName, ';' );
        std::getline( randElem, bonus,   ';' );
        modSelection.push_back(modName);
        bonusSelection.push_back(bonus);
    }
    std::stringstream armor_unparsed;
    std::string _arm[4];
    std::vector<Armor> arms;
    for (const auto &arm : selection) {
        armor_unparsed = std::stringstream(arm);
        std::getline( armor_unparsed, _arm[0], ';' ); // name
        std::getline( armor_unparsed, _arm[1], ';' ); // DEF
        _arm[2] = modSelection.back();
        _arm[3] = bonusSelection.back();
        modSelection.pop_back();
        bonusSelection.pop_back();
        arms.push_back( Armor(_arm[0], stoi(_arm[1]), _arm[2], stoi(_arm[3])) );
    }
    this->armor = arms;
}

Weapon Shop::buyWeapon(const size_t i) {
    Weapon wep = this->weapons[i];
    this->weapons.erase(std::ranges::cbegin(this->weapons) + i);
    return wep;
}

Armor Shop::buyArmor(const size_t i) {
    Armor arm = this->armor[i];
    this->armor.erase(std::ranges::cbegin(this->armor) + i);
    return arm;
}

std::string Shop::getWepInfo(const size_t i) const {
    if ( std::ranges::size(this->weapons) == 0 ) {
        return " -- ";
    }
    auto w = this->weapons[i];
    return (w.getFullName() + ": " +
            std::to_string(w.Item::getStat()) + " ATK, " +
            std::to_string(w.getAtkSpeed()) + " SPD ");
}

std::string Shop::getArmInfo(const size_t i) const {
    if ( std::ranges::size(this->armor) == 0 ) {
        return " -- ";
    }
    auto a = this->armor[i];
    return (a.getFullName() + ": " +
            std::to_string(a.Item::getStat()) + " DEF ");
}

unsigned int Shop::getWepCost(const size_t i) const {
    if ( std::ranges::size(this->weapons) != 0 ) {
        return this->weapons[i].getCost();
    }
    return 0;
}

unsigned int Shop::getArmCost(const size_t i) const {
    if ( std::ranges::size(this->armor) != 0 ) {
        return this->armor[i].getCost();
    }
    return 0;
}

Shop::Shop() {
    this->loadWeapons(0);
    this->loadArmor(0);
}
