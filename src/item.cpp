#include "headers/item.hpp"

/*
** -= ITEM CLASS =-
*/
std::string Item::getName() const {
    return this->name;
}

std::string Item::getModifier() const {
    return this->modifier;
}

std::string Item::getFullName() const {
    return this->modifier + " " + this->name;
}

unsigned int Item::getStat() const {
    return this->stat;
}

unsigned int Item::getCost() const {
    return this->cost;
}

int Item::getBonus() const {
    return this->bonus;
}

Item::Item(const std::string _name, const unsigned int _stat)
    : name(_name), stat(_stat), cost(_stat * 100), modifier("Normal") {}

Item::Item(const std::string _name,
           const unsigned int _stat,
           const std::string _modifier,
           const int _bonus)
    : name(_name), stat(_stat + _bonus), cost((_stat+(_bonus/2)) * 100), modifier(_modifier), bonus(_bonus) {
}

Item::~Item() = default;


/*
** -= WEAPON CLASS =-
*/
unsigned int Weapon::getAtkSpeed() const {
    return this->atkSpeed;
}

Weapon::Weapon(const std::string _name, const unsigned int _stat, const unsigned int _atkSpeed)
    : Item(_name, _stat), atkSpeed(_atkSpeed) {
    this->cost = (((_stat+_atkSpeed) * 100) / 2);
}

Weapon::Weapon(const std::string _name,
               const unsigned int _stat,
               const unsigned int _atkSpeed,
               const std::string _modifier,
               const int _bonus)
    : Item(_name, _stat, _modifier, _bonus), atkSpeed(_atkSpeed) {
    this->cost = (((_stat+_atkSpeed+(_bonus/2)) * 100) / 2);
}

Weapon::~Weapon() = default;


/*
** -= ARMOR CLASS =-
*/
Armor::Armor(const std::string _name, const unsigned int _stat)
    : Item(_name, _stat) {
    // this->cost = _stat * 100;
}

Armor::Armor(const std::string _name,
             const unsigned int _stat,
             const std::string _modifier,
             const int _bonus)
    : Item(_name, _stat, _modifier, _bonus)
{}

Armor::~Armor() = default;
