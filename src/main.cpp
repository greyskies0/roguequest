#include "headers/game.hpp"
#include "headers/shop.hpp"

#define RUN true

int main() {

    Game game;
    if (RUN) {
        //game.showTitle();
        game.start();
    } else { // for testing
    }

    return EXIT_SUCCESS;

}
