\babel@toc {polish}{}\relax 
\babel@toc {english}{}\relax 
\contentsline {section}{\numberline {1}Project's topic}{2}{section.1}%
\contentsline {section}{\numberline {2}Analysis}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}Class selection}{2}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Libraries and algorithms}{3}{subsection.2.2}%
\contentsline {section}{\numberline {3}Data structures and algorithms}{4}{section.3}%
\contentsline {section}{\numberline {4}External specification}{4}{section.4}%
\contentsline {subsection}{\numberline {4.1}User manual}{4}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}User Interface}{4}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Screenshots of the UI}{5}{subsubsection.4.2.1}%
\contentsline {subsection}{\numberline {4.3}Functions available to the user}{7}{subsection.4.3}%
\contentsline {section}{\numberline {5}Internal specification}{8}{section.5}%
\contentsline {subsection}{\numberline {5.1}Relationships}{8}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Important methods}{9}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Overview of the most important classes}{14}{subsection.5.3}%
\contentsline {subsubsection}{\numberline {5.3.1}Player}{14}{subsubsection.5.3.1}%
\contentsline {subsubsection}{\numberline {5.3.2}World}{14}{subsubsection.5.3.2}%
\contentsline {subsubsection}{\numberline {5.3.3}Rand}{14}{subsubsection.5.3.3}%
\contentsline {subsubsection}{\numberline {5.3.4}FileHandler}{15}{subsubsection.5.3.4}%
\contentsline {subsubsection}{\numberline {5.3.5}Game}{15}{subsubsection.5.3.5}%
\contentsline {subsection}{\numberline {5.4}General scheme of the game's operation}{15}{subsection.5.4}%
\contentsline {subsection}{\numberline {5.5}Used techniques from the thematic classes}{16}{subsection.5.5}%
\contentsline {subsection}{\numberline {5.6}Used Object Oriented techniques}{16}{subsection.5.6}%
\contentsline {section}{\numberline {6}Testing, debugging and remarks}{16}{section.6}%
\contentsline {section}{\numberline {7}External libraries}{17}{section.7}%
