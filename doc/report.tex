\documentclass[12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english, polish]{babel}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{titlesec}
\usepackage{graphicx}
\graphicspath{{./img/}}
\usepackage{float}
\usepackage{listings}
\usepackage[export]{adjustbox}
\usepackage{xcolor}
\lstset{ %
    language=C++,
    backgroundcolor=\color{black!5},
    basicstyle=\scriptsize,
}

\titleformat{\subsubsection}
{\bfseries}
{}
{0em}
{}

\begin{document}

\begin{titlepage}
    \centering
    {\scshape\LARGE Politechnika Śląska

    Wydział Automatyki, Elektroniki i Informatyki\par}
    \vspace{1cm}
    {\scshape\Large Computer Programming 4\par}
    \vspace{1.5cm}
    {\huge\bfseries Project report\par}
    \vspace{2cm}
    \vfill
    author: Maciej Krzyżowski\par
    instructor: mgr Wojciech Dudzik\par
    year: 2021/2022
    \vfill
\end{titlepage}

\selectlanguage{english}

\renewcommand\contentsname{Table of contents}
\tableofcontents
\newpage

\section{Project's topic}
My chosen project’s topic will be a role-playing text game called `Rogue Quest', meaning the player will create his own character and will develop him in his chosen way. The user will have freedom of choice, regarding his equipment and actions and will stay informed about the world’s events by messages appearing in a dedicated window.

The game will contain a system of equipment, such as weapons and armor, which will define the character’s statistics and abilities. Additionally, the player will be able to decide, which location to travel to, which results in encountering different enemies and difficulty levels. The game’s goal is to defeat a boss from every location and with each victory, different parts of the story will reveal, but before that the player will have to train his character on weaker opponents and thus increase his statistics and gain better equipment.

Role-playing games assume that the player creates his own fictional character and is taking part in his progress, choosing his equipment and progressing in his statistics. These games are set in a fictional world with a unique story and the player usually can decide on the game's pace and style on his own.

\section{Analysis}

\subsection{Class selection}\label{sec:classes}
I have created the following classes for this game:
\begin{itemize}
  \item{Item --- represents an item from the game. It's the parent class of:}
    \begin{itemize}
      \item{Armor}
      \item{Weapon}
    \end{itemize}
  \item{NPC --- represents an enemy or a boss for the player to fight}
  \item{Player --- represents the player's character }
  \item{Location --- represent one of the locations of the game}
  \item{World --- contains every location from the game and keeps track of defeated bosses}
  \item{Shop --- allows the player to obtain new gear}
  \item{Game --- represents the whole game and connects every class together}
  \item{FileHandler --- used as a helper for file operations}
  \item{Logger --- used for debugging into a text file due to inability to output to the console or use a debugging tool}
  \item{Random --- contains static methods for generating random numbers of container's elements}
  \item{MyException --- custom exception created for different game errors}
\end{itemize}

\subsection{Libraries and algorithms}
The project uses these libraries:
\begin{itemize}
  \item{<algorithm>}
  \item{<array>}
  \item{<exception>}
  \item{<filesystem>}
  \item{<fstream>}
  \item{<iostream>}
  \item{<map>}
  \item{<math.h>}
  \item{<mutex>}
  \item{<random>}
  \item{<ranges>}
  \item{<regex>}
  \item{<sstream>}
  \item{<string>}
  \item{<thread>}
  \item{<vector>}
\end{itemize}

\section{Data structures and algorithms}
The project uses std::vector, std::array and std::map data structures. The vectors are, for example, used to store the player's equipment or the world's locations with their enemies, as they are then used in the UI, which specifically requires the use of vectors, to display them. The map is used in the Player and NPC classes to store statistics.

There are no advanced and complicated algorithms, as the project's operation is simple and doesn't involve heavy calculations. Most of them are either from the standard or <algorithm> library.

\section{External specification}

\subsection{User manual}
Upon entering the game, the user will be presented with the title screen and, shortly after, with the main window. The user can navigate around with the keyboard and mouse. UI elements are either selected (e.g.\ radioboxes) or clicked (buttons).

\subsection{User Interface}
The User Interface will be in a form of an interactive terminal window. It will consist of tabs, which will hold different contents, so that the view won’t be cluttered, and the user will have easy access to a desired window. This interface is provided by the FTXUI library.

(\url{https://github.com/ArthurSonzogni/FTXUI})

\newpage

\subsubsection{Screenshots of the UI}

\begin{center}

\begin{figure}[h!]
  \includegraphics[width=0.9\textwidth, center]{world}
  \caption{Figure presenting the world view}
\end{figure}

\begin{figure}[h!]
  \includegraphics[width=0.9\textwidth, center]{shop}
  \caption{Figure presenting the shop view}
\end{figure}

\newpage

\begin{figure}[h!]
  \includegraphics[width=0.9\textwidth, center]{player}
  \caption{Figure presenting the player view}
\end{figure}

\begin{figure}[h!]
  \includegraphics[width=0.9\textwidth, center]{menu}
  \caption{Figure presenting the menu view}
\end{figure}

\newpage

\begin{figure}[h!]
  \includegraphics[width=0.9\textwidth, center]{fight}
  \caption{Figure presenting the fighting view}
\end{figure}

\end{center}

\subsection{Functions available to the user}
The user will be presented with the following actions to perform:
\begin{itemize}
  \item{Fight a random opponent from the current location}
  \item{Fight a boss from the current location}
  \item{Rest to retrieve Health Points in two ways: either losing Gold or Experience Points}
  \item{Go to a shop to buy new gear}
  \item{Travel to available locations}
  \item{Begin a new game}
  \item{Save or load the game}
  \item{Exit the game}
\end{itemize}

\section{Internal specification}
The game's implementation consists of classes listed and described in the \hyperref[sec:classes]{Class selection} section.

\subsection{Relationships}
The following diagram shows relations between these classes:
\begin{figure}[h]
\includegraphics[width=\textwidth, center]{class_hierarchy}
\end{figure}

\newpage

\subsection{Important methods}
\begin{lstlisting}
// file:    location.cpp
// purpose: Generating an enemy for a specific location
// comment: This method uses a static method from the Rand class
//          to generate a random enemy from a pool of available
//          opponents and then assign somewhat random statistics,
//          which scale to location difficulty
NPC Location::generateEnemy() const {
  std::string enemyName =
    Rand::element(this->availableEnemies);
  unsigned int locDifficulty = this->getDifficulty();
  unsigned int enemyBaseStat = 3 * locDifficulty;
  unsigned int enemyHP = 50 + (35 * locDifficulty) +
                         Rand::range(locDifficulty*5);

  std::map<std::string, int> enemyStats {
    {"HP",    enemyHP},
    {"maxHP", enemyHP},
    {"ATK",   enemyBaseStat +
              (Rand::range(locDifficulty)+1)/2},
    {"DEF",   enemyBaseStat +
              (Rand::range(locDifficulty)+1)/2},
    {"SPD",   enemyBaseStat +
              (Rand::range(locDifficulty)+1)/2},
  };

  return NPC(enemyName, enemyStats);
}
\end{lstlisting}

\begin{lstlisting}
// file:    world.cpp
// purpose: Loading locations from a text file
// comment: This method uses regex to validate the format of stored
//          locations and then uses the data from the text file to
//          create the world's locations. The data is retrieved
//          using the FileHandler class.
void World::loadLocations() {
  namespace fs = std::filesystem;

  std::regex correct_loc_name("^[a-zA-Z ]*$");
  std::regex correct_loc_diff("^\\d*$");
  std::regex correct_loc_enemies("^([a-zA-Z ]*;?)*$");
  std::regex correct_loc_boss("^[a-zA-Z ]* Boss$");

  std::string locName = "";
  unsigned int locDiff = 0;
  std::stringstream enemiesList;
  std::string enemy;
  std::vector<std::string> enemies = {};
  std::string bossName;
  fs::path locFile = fs::current_path() / "assets/locations.txt";
  if ( fs::exists(locFile) ) {
    auto fileContents = FileHandler::fGetLines(locFile.string());
    if ( std::ranges::size(fileContents) % 4 != 0 ) {
      Logger::log(...); // custom message is in place of '...'
      throw MyException(...);
      return;
    }
    for (size_t i = 0; i < std::ranges::size(fileContents); i += 4) {
      enemies.clear();
      if ( !std::regex_match(fileContents[i], correct_loc_name)  ||
         !std::regex_match(fileContents[i+1], correct_loc_diff)  ||
         !std::regex_match(fileContents[i+2], correct_loc_enemies) ||
         !std::regex_match(fileContents[i+3], correct_loc_boss) ) {
        throw MyException(...);
      }
      locName = fileContents[i];
      locDiff = std::stoi(fileContents[i+1]);
      enemiesList = std::stringstream(fileContents[i+2]);
      while ( std::getline(enemiesList, enemy, ';') ) {
        enemies.push_back(enemy);
      }
      bossName = fileContents[i+3];
      this->locations.push_back(Location(locName,
                                         locDiff,
                                         enemies,
                                         bossName));
    }
  } else {
    Logger::log(...);
    throw MyException(...);
  }
  std::vector<std::string> logs;
  logs.emplace_back("Loaded locations:");
  for (auto l : this->locations) {
    logs.push_back(" > " + l.getName());
    for (auto e : l.availableEnemies) {
      logs.push_back("   > " + e);
    }
  }
  Logger::log(logs);
}
\end{lstlisting}

\begin{lstlisting}
// file:    game.cpp
// purpose: Attacks between the Player and NPCs during the fight
// comment: These two methods describe how fighting is implemented.
//          The attackEnemy() method executes once per player's button
//          click, unless he has no Action Points left, while attackPlayer()
//          executes until NPCs have any of their Action Points.
void Game::attackEnemy(const size_t i) {
  try {
    if ( this->in_fight || this->player.getHP() <= 0 ) {
      return;
    }

    bool is_boss =
      ( this->enemies[i].getName().find("Boss") != std::string::npos );

    if ( this->playerAP > 0 && !std::ranges::empty(this->enemies)
         && this->player.getHP() > 0 ) {
      auto ATK  = this->player.getATK(this->selectedWeapon);
      auto DEF  = this->enemies[i].getStat("DEF");
      auto DMG  = (( ATK + (ATK / DEF) ) * DMG_MULTIPLIER) + Rand::range(2);
      auto name = this->enemies[i].getName();
      try {
        this->enemies[i].takeDamage(DMG);
      } catch (MyException &e) {
        this->screen.ExitLoopClosure();
        std::cout << e.what() << '\n';
        return;
      }
      this->fightMsg = name + " has taken " +
        std::to_string(DMG) + " damage!";
      this->playerAP -= 1;
      if ( this->enemies[i].getStat("HP") <= 0 ) {
        this->enemies.erase(std::ranges::cbegin(this->enemies) + i);
        const unsigned int diff  = this->world.currLocation.getDifficulty();
        const unsigned int currEXP = this->player.getEXP();
        const double penalty =
          double( diff * 500 < currEXP ? currEXP / 200 : 1 );
        int gainedGold =
          5 * diff * (10 + Rand::range(3));
        gainedGold =
          ( is_boss ? gainedGold * 6 : gainedGold );
        int gainedEXP =
          3 * diff * (20 + Rand::range(2));
        gainedEXP =
          ( is_boss ? gainedEXP  * 6 : gainedEXP ) / penalty;
        this->player.addGold(gainedGold);
        this->player.addEXP(gainedEXP);
        this->player.tryLevelUp();
        if ( std::ranges::empty(this->enemies) ) {
          this->fightMsg += " You have won the battle! Rewards: " +
            std::to_string(gainedGold) + "g, " +
            std::to_string(gainedEXP) + "xp";
          this->in_fight = false;
          if ( is_boss ) {
            this->world.killBoss(this->world.getCurrLocationIndex());
            this->eventMessage =
              this->world.getStory(this->world.getCurrLocationIndex());
            this->updateShop();
          }
        }
      }
    } else if ( std::ranges::empty(this->enemies) ) {
      this->fightMsg = "Press 'END TURN' to exit the fight";
    } else if ( this->playerAP <= 0 ) {
      this->fightMsg = "You don't have any Action Points left!";
    }
  } catch (MyException &e) {
    this->popup_index = 0;
    this->screen.ExitLoopClosure();
    std::cout << e.what() << '\n';
    return;
  } catch (std::exception e) {
    this->popup_index = 0;
    this->screen.ExitLoopClosure();
    std::cout << e.what() << '\n';
    return;
  } catch (...) {
    this->popup_index = 0;
    this->screen.ExitLoopClosure();
    return;
  }
}

void Game::attackPlayer() {
  using namespace std::chrono_literals;
  try {
    if ( this->in_fight ) {
      return;
    }
    this->in_fight = true;
    if ( std::ranges::empty(this->enemies) ) {
      this->in_fight = false;
      this->popup_index = 0;
      return;
    }
    auto fight_thread = std::thread([&] {
      for ( const auto& e : enemies ) {
        auto ATK = e.getStat("ATK");
        auto DEF = this->player.getDEF(this->selectedArmor);
        auto DMG = ( ATK + (ATK/DEF) ) * DMG_MULTIPLIER;
        auto DMG_total = DMG;
        for ( int i = 0; i < e.getStat("SPD"); ++i ) {
          DMG_total = DMG + Rand::range(2);
          this->player.takeDamage(DMG_total);
          this->fightMsg = "You have taken "
            + std::to_string(DMG_total)
            + " damage from "
            + e.getName() + "!";
          std::this_thread::sleep_for(1s);
          if ( this->player.getHP() <= 0 ) {
            break;
          }
        }
      }
      this->fightMsg += " *END OF ENEMY TURN*";
      this->playerAP = this->player.getSPD(this->selectedWeapon);
      this->in_fight = false;
      if ( this->player.getHP() <= 0 ) {
        this->fightMsg = "You have lost the battle!";
        this->player.addGold( -(this->player.getGold() / 5) );
        this->player.addEXP ( -(this->player.getEXP()  / 5) );
        std::this_thread::sleep_for(2s);
        this->popup_index = 0;
        return;
      }
    });
    fight_thread.detach();
  } catch (MyException &e) {
    this->screen.ExitLoopClosure();
    std::cout << e.what() << '\n';
    return;
  } catch (std::exception e) {
    this->screen.ExitLoopClosure();
    std::cout << e.what() << '\n';
    return;
  } catch (...) {
    this->screen.ExitLoopClosure();
    return;
  }
}
\end{lstlisting}

\begin{lstlisting}
// file:    game_start.cpp
// method:  void Game::start()
// purpose: This method is responsible for the whole UI
// comment: This is a part of the Game::start() method,
//          which represents the 'Menu' tab of the UI.
//          Here it is presented to show how the UI is built.
auto new_game_button = ButtonOption();
auto save_button =     ButtonOption();
auto load_button =     ButtonOption();
auto getSaves = []() {
  std::vector<std::string> out;
  for (auto const &save :
       fs::directory_iterator{fs::current_path() / "saves"}) {
    out.push_back(save.path().filename().string());
  }
  std::sort(std::ranges::begin(out), std::ranges::end(out));
  out.erase(std::ranges::begin(out));
  return out;
};
std::vector<std::string> saveNames = getSaves();
int s_idx = 0;
auto saveMenu = Menu(&saveNames, &s_idx);

auto MenuView = Container::Vertical({
  Button(" [ NEW GAME ] ", [&] {
    this->load("*new*");
    tab_index = 0;
  }, &new_game_button) | hcenter,
  Button(" [ SAVE ] ", [&] {
    this->save(saveNames[s_idx]);
    tab_index = 0;
  }, &save_button) | hcenter,
  Button(" [ LOAD ] ", [&] {
    this->load(saveNames[s_idx]);
    tab_index = 0;
  }, &load_button) | hcenter,
  Button(" [ EXIT ] ", this->screen.ExitLoopClosure()) | bold | hcenter,
  saveMenu | hcenter
});


auto Menu = Renderer(MenuView, [&] {
  auto menu_win = window(text("Menu") | bold,
               MenuView->Render() | hcenter | frame | flex);

  return vbox({
    menu_win | flex,
  }) | flex_grow | border;
});
\end{lstlisting}

\subsection{Overview of the most important classes}

\subsubsection{Player}
This is a class representing the player's character. It consists of a map containing player's statistics, vectors which hold the equipment and amount of collected gold. It has getter and setter methods, and methods used for actions like resting, fighting, obtaining new equipment or increasing maxHP statistic due to gained Experience Points.

Player's statistics are: HP (Health Points), maxHP (maximum amount of Health Points), EXP (Experience Points), and statistics gained from equipment are: ATK (Attack Points), DEF (Defense Points), SPD (Weapon's speed).

\subsubsection{World}
The World class holds an instance of every available location and keeps track of defeated bosses. During the creation of the locations, their names, difficulty levels, enemies' and bosses' names are assigned. The class contains getter methods, as well as methods for changing current world state (current location or marking a boss as defeated).

\subsubsection{Rand}
This class fully consists of static methods, which are used for:
\begin{itemize}
  \item{Getting a random element from a vector with various random distributions}
  \item{Getting a random value from 0 to 100, used for calculating the chance of fleeing a battle}
  \item{Generating a value from -x to x}
  \item{Generating a random value from 0 to x}
\end{itemize}
These static methods are used during fights or in generating an opponent and rewards after fight.

\subsubsection{FileHandler}
Similarly to the Rand class, it contains static methods to make the file operations easier by extracting file's contents and returning them as a vector.

\subsubsection{Game}
The Game class is the main driver of the game's functionality. It takes care of every action performed by the player and the creation of the User Interface. It holds the Player and World objects and implements the most important methods accessed by the UI's elements.

\subsection{General scheme of the game's operation}
Upon starting the game, the `World' tab is shown to the player, containing available actions, locations to travel to and a message window, which will inform about taken actions and their consequences. The player can also access the `Player', `Shop' and `Menu' tab, which contain the player's statistics and equipment, new weapons and armor to buy and options to save, load, exit or start a new game, respectfully. The player is free to choose what and when to do.

If the player decides to fight an opponent or a boss, a pop-up window appears with the fight's details. The player is presented with 3 gauges: enemies' Health Points and player's Health and Action Points, 3 buttons to attack, end the turn and flee the battle and a textbox for information about the fight in progress. After a successful fight the player's character gains gold and experience points based on the difficulty level and the view returns from the pop-up to the previous window. The experience points he gains are reduced if player's experience points greatly surpass the location's difficulty level to prevent the game from being too easy.

The game is mainly about progressing by defeating opponents, which results in the ability to fight the bosses. Upon defeating any boss, some part of the story is presented on the screen. The game ends by defeating the last boss with a farewell message to the player.

\newpage

\subsection{Used techniques from the thematic classes}
The game uses following techniques:
\begin{itemize}
  \item{filesystem --- saving and loading functionality}
  \item{regex --- checking if saves' or assets' file contents are valid}
  \item{ranges --- operations on vectors}
  \item{thread-related libraries --- concurrent and asynchronous events in the game, synchronization with mutexes}
\end{itemize}

\subsection{Used Object Oriented techniques}
As the object oriented techniques, the game uses inheritance (Item class as a parent class), function overloading, data encapsulation, and keeping the objects related to each other (e.g.\ in the form of composition).

\section{Testing, debugging and remarks}
During the creation of the game I have encountered several bugs and errors, mainly caused either by being unexperienced with the chosen UI library and UI libraries in general. Fortunately I have eliminated every bug so far, that I am aware of. For the debugging I have used the Logger and MyException classes. The only problem left, that I can't find the solution to, is the inability to refactor the Game::start() function (responsible for the UI) and split it to several smaller ones due to the game crashing upon every attempt (with the `core dump' message). I believe it's caused by the characteristic of the UI library, where the visual elements heavily depend on the data they would show and this data must have the lifetime of the whole game loop.
Additionally, the game has problems compiling on Windows (it is possible but the MSVC started to output linker errors on the later stages of the project). Due to my only computer having Windows breaking recently, I'm not able to fix it. Running it on Linux is recommended.

\section{External libraries}
One external library in use is the FTXUI library (\url{https://github.com/ArthurSonzogni/FTXUI}). It is signed under the MIT License and is used in the creation of the user interface. I have selected it because of it being well documented, flexible, cross-platform and easy to build with tools such as CMake.


\end{document}
