#!/usr/bin/env sh

clean()
{
    mv build/README.md ./tmp && cd build && rm -rfv -- * && mv ../tmp ./README.md && cd ..
}

build()
{
    clear && cd build && cmake .. && make && cd ..
}

default()
{
    clear && cd build && cmake .. && make && cd .. && ./run
}

case "$1" in
    "--clean") clean   ;;
    "--build") build   ;;
    *)         default ;;
esac
